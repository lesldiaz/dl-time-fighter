package com.example.intro_moviles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var textoVistaMensajeBienvenida: TextView
    private lateinit var subtextoVistaMensajeBienvenida: TextView
    private lateinit var btnCambiarTexto: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textoVistaMensajeBienvenida = findViewById(R.id.mensaje_bienvenida)
        subtextoVistaMensajeBienvenida = findViewById(R.id.otro_mensaje)
        btnCambiarTexto = findViewById(R.id.btn_cambiar_texto)
        btnCambiarTexto.setOnClickListener{cambiarMensajeBienvenidaSubtitulo()}

    }

    private fun cambiarMensajeBienvenidaSubtitulo(){
        textoVistaMensajeBienvenida.text = "Quiero guardar este momento en mi corazon"
        subtextoVistaMensajeBienvenida.text = "Texto cambiado xdxd"
    }


}
