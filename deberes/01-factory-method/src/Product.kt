package factory.method

interface Archivo

class ArchivoXml : Archivo {
    constructor(){
        print("Este es un archivo xml")
    }
}
class ArchivoJson : Archivo {
    constructor(){
        print("Este es un archivo json")
    }
}
fun main() {
    val parserFactory = ArchivoFactoryStandart()
    parserFactory.crearArchivo("datos.json")
}