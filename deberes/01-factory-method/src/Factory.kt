package factory.method

import java.lang.Exception

interface ArchivoFactory {
    fun crearArchivo(nombreArchivo: String): Archivo
}

class ArchivoFactoryStandart : ArchivoFactory {
    override fun crearArchivo(nombreArchivo: String) =
        when (nombreArchivo.substringAfterLast('.')) {
            "xml" -> ArchivoXml()
            "json" -> ArchivoJson()
            else -> throw Exception("El archivo $nombreArchivo no ha sido reconocido.")
        }
}